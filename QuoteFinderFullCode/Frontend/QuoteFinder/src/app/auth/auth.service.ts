import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { MatDialog } from "@angular/material/dialog";
import { MessageComponent } from "../message/message.component";
import { AuthData } from './auth-data.model';
import { environment } from '../../environments/environment';

const BACKEND_URL = environment.apiUrl+'/Authenticate/';

@Injectable({providedIn:'root'})

export class AuthService {


  private token: string = "";
  private tokenTimer: any;
  private userId: string = "";
  private role: string = "";
  authStatusListener = new Subject<AuthData>();
  isAuthenticated = false;


  constructor(private http: HttpClient, private router: Router, private dialog: MatDialog){}

  getToken(){

    const authInformation: any = this.getAuthData();
    if(!authInformation){
      return null;
    }

    this.token = authInformation.token;

    return this.token;
  }

  getIsAuth(){


    this.autoAuthUser();

    const authInformation: any = this.getAuthData();
    if(!authInformation){

      this.isAuthenticated = false;

      return this.isAuthenticated;
    }

    this.isAuthenticated = true;

    return true;

  }

  getUserId(){
    return this.userId;
  }

  getAuthStatusListener(){
    return this.authStatusListener.asObservable();
  }

  createUser(authdata: any){

    const authData: any = {username: authdata.username,
                          password: authdata.password,
                          email: authdata.email,
                          contactNo: authdata.contactNo,
                          fullName: authdata.fullName,
                          DOB: authdata.DOB,
                          address: authdata.address,
                          };

    //console.log(authData);

    this.http.post(BACKEND_URL+'register', authData).subscribe( response => {
      //console.log(response);
      authData.isAuthenticated = true;
      this.authStatusListener.next(authData);
      this.dialog.open(MessageComponent, {data: {messageTitle:"Success" ,message: "Signup Sucessfull."} });
      this.router.navigate(['/auth/login']);


    }, error => {
      //console.log(error.error.message);
      authData.isAuthenticated = false;
      this.authStatusListener.next(authData);
      this.dialog.open(MessageComponent, {data: {messageTitle:"Error", message: error.error.message} });
    });


  }

  login(username: string, password: string){

    const authData: any = {username: username, password: password, isAuthenticated: false};

    this.http.post<{token: string, expiration: string, role: string, userName: string, userId: string }>(BACKEND_URL+'login', authData).subscribe( response => {

      console.log(response);
      const token = response.token;
      this.token = token;

      //console.log(token);

      if(this.token){

        this.userId = response.userId;
        this.role = response.role;
        this.isAuthenticated = true;
        authData.isAuthenticated = true;
        const expirationDate = new Date(Date.parse(response.expiration));
        this.saveAuthData(token, expirationDate, this.userId, this.role);
        this.authStatusListener.next(authData);
        this.setAuthTimer(180*60);
        this.router.navigate(['/quotes']);
      }

      //console.log(this.token);

    }, error => {
      this.authStatusListener.next(authData);
      this.dialog.open(MessageComponent, {data: {messageTitle:"Error" ,message: "Invalid Credentials!!"} });
    });

  }

  autoAuthUser(){

    const authInformation: any = this.getAuthData();
    if(!authInformation){
      return;
    }

   const now  = new Date();
   const expiresIn = (new Date(Date.parse(authInformation.expirationDate)).getTime() - now.getTime())/1000;

   //const expiresIn = 180*60;
   //console.log("exppppppppppppppppppppppp");
   //console.log(expiresIn);
   //console.log("exppppppppppppppppppppppp");

    if(expiresIn > 0){
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.userId = authInformation.userId;
      this.setAuthTimer(expiresIn);
      const authData: any = { userId : this.userId , isAuthenticated : true }
      this.authStatusListener.next(authData);

    }
  }

  logout(){

    this.token = "";
    this.isAuthenticated = false;
    const authData: any = { isAuthenticated : false }
    this.authStatusListener.next(authData);
    clearTimeout(this.tokenTimer);
    this.userId = "";
    this.clearAuthData();
    this.router.navigate(['/auth/login']);

  }

  private setAuthTimer(duration: number){

    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);

  }

  private saveAuthData(token: string, expirationDate: Date, userId: string, role: string){

    localStorage.setItem("token", token);
    localStorage.setItem("expiration", ""+expirationDate);
    localStorage.setItem("userId", userId);
    localStorage.setItem("role", role);

  }

  private clearAuthData(){
    localStorage.removeItem("token");
    localStorage.removeItem("expiration");
    localStorage.removeItem("userId");
    localStorage.removeItem("role");
  }

  public getAuthData(){
    const token = localStorage.getItem("token");
    const expirationDate = localStorage.getItem("expiration");
    const userId = localStorage.getItem("userId");
    const role = localStorage.getItem("role");

    if(!token || !expirationDate){
      return;
    }

    return {
     "token": token,
     "expirationDate": new Date(expirationDate),
     "userId": userId,
     "role": role
    };

  }



}
