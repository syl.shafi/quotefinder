import { Component, OnInit,  OnDestroy} from "@angular/core";
import { NgForm, FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { AuthService } from "../auth.service";

interface Gender {
  value: string;
  viewValue: string;
}

interface Type {
  value: string;
  viewValue: string;
}

@Component({
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})


export class SignupComponent implements OnInit, OnDestroy{


  isLoading = false;
  username: String = ""
  password: String = ""
  email: String = ""
  contactNo: String = ""
  fullName: String = ""
  DOB: String = ""
  address: String = ""

  authStatusSub: Subscription = new Subscription();

  constructor(public authService: AuthService){

  }


  ngOnInit(){

   this.authStatusSub = this.authService.getAuthStatusListener().subscribe(
      data => {
        this.isLoading = false;
        this.username = data.username,
        this.password = data.password,
        this.email = data.email,
        this.contactNo = data.contactNo,
        this.fullName = data.fullName,
        this.DOB = data.DOB,
        this.address = data.address
      }
    );
  }

  onSignup(form: NgForm){

    if(form.invalid){
      return;
    }

    if(form.value.confirmPassword != form.value.password){
      return;
    }

    this.isLoading = true;

    this.authService.createUser(form.value);

  }

    ngOnDestroy(){
    this.authStatusSub.unsubscribe();
  }

}
