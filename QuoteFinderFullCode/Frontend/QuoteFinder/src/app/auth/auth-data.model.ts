export interface AuthData {
  userId: string;
  username: string;
  password: String;
  email: String;
  contactNo: String;
  fullName: String;
  DOB: String;
  address: String;
  blocked: boolean;
  role: string;
  isAuthenticated: boolean;
}
