import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AddQuoteComponent } from './quote/addQuote/add-quote.component';
import { EditQuoteComponent } from './quote/editQuote/edit-quote.component';
import { QuoteListComponent } from './quote/quoteList/quote-list.component';
import { SettingComponent } from './quote/quoteSetting/setting-quote.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'auth/login', component: LoginComponent},
  {path: 'auth/signup', component: SignupComponent},
  {path: 'quotes', component: QuoteListComponent, canActivate: [AuthGuard]},
  {path: 'quotes/add', component: AddQuoteComponent, canActivate: [AuthGuard]},
  {path: 'quotes/edit/:id', component: EditQuoteComponent, canActivate: [AuthGuard]},
  {path: 'quotes/settings', component: SettingComponent, canActivate: [AuthGuard]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
