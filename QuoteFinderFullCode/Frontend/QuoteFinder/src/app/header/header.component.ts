import {Component} from '@angular/core'
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']

})

export class HeaderComponent {

  private authListenerSub: Subscription = new Subscription();
  userIsAuthenticated = false;
  role = "";
  authdata: any = null;
  colapse: any = false;

  constructor(public authService: AuthService){

  }

  ngOnInit(){


      this.userIsAuthenticated = this.authService.getIsAuth();


      this.authListenerSub = this.authService.getAuthStatusListener().subscribe(data => {
        this.userIsAuthenticated = data.isAuthenticated;

        this.authdata = this.authService.getAuthData();

        if(this.authdata != null){

          this.role =  this.authdata.role;

        }

      });

  }

  onLogout(){

    this.authService.logout();

  }

  onColapseChange(){

    this.colapse = !this.colapse;

  }

  ngOnDestroy(){

    this.authListenerSub.unsubscribe;

  }


}
