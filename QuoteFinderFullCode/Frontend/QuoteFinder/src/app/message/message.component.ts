import { Component, Inject, OnDestroy, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  templateUrl: './message.component.html',
})

export class MessageComponent {

   constructor(@Inject(MAT_DIALOG_DATA) public data: {messageTitle: string, message: string,}){

   }

}
