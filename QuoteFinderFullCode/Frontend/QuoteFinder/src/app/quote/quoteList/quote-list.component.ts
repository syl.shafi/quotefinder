import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { NgForm } from "@angular/forms";
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { QuoteData, VisibilityStatus } from '../quote-data.model';
import { QuoteService } from '../quote.service';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';


interface Option {
  value: string;
  viewValue: string;
}

@Component(
  {
    selector: 'app-quote-list',
    templateUrl: './quote-list.component.html',
    styleUrls : ['./quote-list.component.css'],

  }
)

export class QuoteListComponent implements OnInit, OnDestroy{

   quotes: QuoteData[] = [];
   private quotesSub: Subscription = new Subscription();
   isLoading = false;
   totalquotes = 0;
   currentPage = 1;
   quotesPerPage = 100;
   pageSizeOptions = [1, 2, 5, 10];
   private authListenerSub: Subscription = new Subscription();
   userIsAuthenticated = false;
   userId: string = "";
   searchText: string = "";
   visibilityStatus: VisibilityStatus = VisibilityStatus.Active;
   VisibilityStatusList: Option[] = [
     {value: VisibilityStatus.Active, viewValue: 'Active'},
     {value: VisibilityStatus.Trashed, viewValue: 'Trashed'},
   ];


   @ViewChild(MatPaginator, { static: false })
  paginator!: MatPaginator;

   constructor(public quoteService : QuoteService, public authService: AuthService, private router: Router){}


   ngOnInit(){

    this.quoteService.getsetting().subscribe((data :any) => {

      this.isLoading = false;
      if(data.disableView){
        this.router.navigate(["/"]);
        return;
      }

      this.isLoading = false;
      this.quoteService.getquotes(this.quotesPerPage, this.currentPage, this.searchText, this.visibilityStatus);
      this.userId = this.authService.getUserId();
      this.quoteService.getquotesUpdateListener().subscribe((data:
        {quotes: QuoteData[], quoteCount: number}) => {
        this.quotes = data.quotes;
        this.totalquotes = data.quoteCount;
        this.isLoading = false;
      });

      this.userIsAuthenticated = this.authService.getIsAuth();
      this.authListenerSub = this.authService.getAuthStatusListener().subscribe(data => {
        this.userIsAuthenticated = data.isAuthenticated;
        this.userId = this.authService.getUserId();
      });

    });

  }

  ngOnDestroy(){
    this.quotesSub.unsubscribe();
    this.authListenerSub.unsubscribe;
  }

  onSearch(form: NgForm){
    this.isLoading = true;
    if(this.paginator){
      this.paginator.pageIndex = 0;
    }
    this.currentPage = 1;
    this.quoteService.getquotes( this.quotesPerPage, this.currentPage, this.searchText, this.visibilityStatus);
  }

  onChangedPage(pageData:PageEvent){
    //console.log(pageData.pageIndex);
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.quotesPerPage = pageData.pageSize;
    this.quoteService.getquotes( this.quotesPerPage, this.currentPage, this.searchText, this.visibilityStatus);

  }

  onDelete(quoteId: String){

    this.quoteService.getsetting().subscribe((data :any) => {

      this.isLoading = false;
      if(data.disableDelete){
        return;
      }

      if(confirm("Are you sure to delete?")){

        this.quoteService.deletequote(quoteId).subscribe(
          resp => {
            this.isLoading = true;
            this.quoteService.getquotes( this.quotesPerPage, this.currentPage, this.searchText, this.visibilityStatus);
          }, error => {
              this.isLoading = false;
          }
        );
      }

    });

  }

  onActive(quoteId: String){

    this.quoteService.getsetting().subscribe((data :any) => {

      this.isLoading = false;
      if(data.disableEdit){
        return;
      }

      if(confirm("Are you sure to recover?")){

        this.quoteService.setStatus(quoteId, "Active").subscribe(
          resp => {
            this.isLoading = true;
            this.quoteService.getquotes( this.quotesPerPage, this.currentPage, this.searchText, this.visibilityStatus);
          }, error => {
              this.isLoading = false;
          }
        );
      }
    });

  }


  onTrash(quoteId: String){

    this.quoteService.getsetting().subscribe((data :any) => {

      this.isLoading = false;
      if(data.disableEdit){
        return;
      }

      if(confirm("Are you sure to move to trash?")){

        this.quoteService.setStatus(quoteId, "Trashed").subscribe(
          resp => {
            this.isLoading = true;
            this.quoteService.getquotes( this.quotesPerPage, this.currentPage, this.searchText, this.visibilityStatus);
          }, error => {
              this.isLoading = false;
          }
        );
      }

    });

  }


}
