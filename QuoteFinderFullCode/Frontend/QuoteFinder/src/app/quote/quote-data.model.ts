export interface QuoteData {

  id: String;
  quoteText: String;
  description: String;
  authorName: String;
  authorTitle: String;
  visibilityStatus: VisibilityStatus;
  entryDate: String;
  lastUpdatedAt: string;
  entryByUserId: String;
  entryByUserName: String;
  modifiedByUserId: String;
  modifiedByUserName: String;

}



export enum VisibilityStatus{
  Active = "Active",
  Trashed = "Trashed",
}




