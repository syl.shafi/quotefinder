import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {QuoteData} from './quote-data.model';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import * as moment from 'moment';

const BACKEND_URL = environment.apiUrl+'/Quote/';

@Injectable({providedIn:'root'})

export class QuoteService{

  private quotes : QuoteData[] = [
  ];
  private quotesUpdated = new Subject<{quotes: QuoteData[], quoteCount: number}>();

  constructor(private http: HttpClient, private router: Router){}


  getquotes(quotesPerPage: number, currentPage:number, searchText : string, visibilityStatus: string){

    const queryParams=`get-all?noOfResults=${quotesPerPage}&index=${currentPage}&search=${searchText}&VisibilityStatus=${visibilityStatus}`;

    this.http.get<{ quotes: any, maxquotes: number}>
    (BACKEND_URL+queryParams)
    .pipe(map(quoteData =>{

      //console.log(quoteData);

      return {
        quotes: quoteData.quotes.map((quote: any) =>{

          //console.log(quote.Images);

          return {
            id: quote.id,
            quoteText: quote.quoteText,
            description: quote.description,
            authorName: quote.authorName,
            authorTitle: quote.authorTitle,
            visibilityStatus: quote.visibilityStatus,
            entryDate: moment(quote.entryDate).format('MMMM Do YYYY, h:mm:ss a'),
            lastUpdatedAt: moment(quote.lastUpdatedAt).format('MMMM Do YYYY, h:mm:ss a'),
            entryByUserId: quote.entryByUserId,
            entryByUserName: quote.entryByUserName,
            modifiedByUserId: quote.modifiedByUserId,
            modifiedByUserName: quote.modifiedByUserName
          };

        }),
        quoteCount: quoteData.maxquotes
      }

    }))
    .subscribe((transformedquoteData)=>{

      this.quotes = transformedquoteData.quotes;
      this.quotesUpdated.next({quotes: [...this.quotes], quoteCount: transformedquoteData.quoteCount});

    });

  }

  getquotesUpdateListener(){
    return this.quotesUpdated.asObservable();
  }

  addquote(quotedata: any){

    this.http.post(BACKEND_URL+"add", quotedata).subscribe( response => {
      //console.log(response);

      this.router.navigate(["/quotes"]);

    });

  }


  getquote(id: string){
    return this.http.get(BACKEND_URL+"get-single/"+ id);
  }

  getsetting(){
    return this.http.get(BACKEND_URL+"get-settings");
  }

  addSetting(data: any){

    this.http.post(BACKEND_URL+"settings", data).subscribe( response => {
      //console.log(response);
      this.router.navigate(["/quotes"]);

    });

  }

  EditQuote(quotedata: any){

    //console.log(id);

    this.http.put(BACKEND_URL+"edit/"+ quotedata.id, quotedata).subscribe(
      (response: any) => {
        this.router.navigate(["/quotes"]);
      }
    );

  }

  deletequote(quoteId: String){

    //console.log(quoteId);

    return this.http.delete(BACKEND_URL+ quoteId);


  }


  setStatus(quoteId: String, VisibilityStatus: String){

    //console.log(quoteId);

    return this.http.post(BACKEND_URL+"set-visibility?id="+ quoteId+"&&VisibilityStatus="+VisibilityStatus, null);


  }




}
