import { Component, OnInit,  OnDestroy} from "@angular/core";
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Subscription } from "rxjs";
import { QuoteService } from "../quote.service";

interface VisibilityStatus {
  value: string;
  viewValue: string;
}


@Component({
  templateUrl: './setting-quote.component.html',
  styleUrls: ['./setting-quote.component.css'],
})


export class SettingComponent implements OnInit, OnDestroy{


  isLoading = false;
  disableAdd: any = "";
  disableEdit: any = "";
  disableDelete: any = "";
  disableView: any = "";

  form: FormGroup = new FormGroup({});


  quoteSub: Subscription = new Subscription();

  constructor(public quoteService: QuoteService, private fb: FormBuilder){

    this.form = fb.group({
      disableAdd: ['', []],
      disableEdit: ['', []],
      disableDelete: ['', []],
      disableView: ['', []],
    })

  }


  ngOnInit(){

    this.quoteSub = this.quoteService.getquotesUpdateListener().subscribe(
      isQuote => {
        this.isLoading = false;
      }
    );

    this.quoteService.getsetting().subscribe((data :any) => {

      this.isLoading = false;

      this.disableAdd = data.disableAdd;
      this.disableEdit = data.disableEdit;
      this.disableDelete = data.disableDelete;
      this.disableView = data.disableView;

      this.form.setValue({
        'disableAdd': this.disableAdd,
        'disableEdit': this.disableEdit,
        'disableDelete': this.disableDelete,
        'disableView': this.disableView,
      });

    });


  }

  onAddSetting(){

    if(this.form.invalid){
      return;
    }

    this.isLoading = true;

    this.quoteService.addSetting(this.form.value);

  }

    ngOnDestroy(){
    this.quoteSub.unsubscribe();
  }

}
