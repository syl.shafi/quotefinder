import { Component, OnInit,  OnDestroy} from "@angular/core";
import { NgForm, FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { QuoteService } from "../quote.service";
import { Router } from '@angular/router';

interface VisibilityStatus {
  value: string;
  viewValue: string;
}


@Component({
  templateUrl: './add-quote.component.html',
  styleUrls: ['./add-quote.component.css'],
})


export class AddQuoteComponent implements OnInit, OnDestroy{


  isLoading = false;

  quoteSub: Subscription = new Subscription();

  constructor(public quoteService: QuoteService, private router: Router){

  }


  ngOnInit(){

    this.quoteService.getsetting().subscribe((data :any) => {

      this.isLoading = false;
      if(data.disableAdd){
        this.router.navigate(["/quotes"]);
      }

      this.quoteSub = this.quoteService.getquotesUpdateListener().subscribe(
        isQuote => {
          this.isLoading = false;
        }
      );

    });



  }

  onaddQuote(form: NgForm){

    if(form.invalid){
      return;
    }

    this.isLoading = true;

    this.quoteService.addquote(form.value);

  }

    ngOnDestroy(){
    this.quoteSub.unsubscribe();
  }

}
