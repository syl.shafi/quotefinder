import { Component, OnInit,  OnDestroy} from "@angular/core";
import { NgForm, FormControl } from "@angular/forms";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { QuoteService } from "../quote.service";

interface VisibilityStatus {
  value: string;
  viewValue: string;
}


@Component({
  templateUrl: './edit-quote.component.html',
  styleUrls: ['./edit-quote.component.css'],
})


export class EditQuoteComponent implements OnInit, OnDestroy{


  isLoading = false;
  id: any = "";
  quoteText: any = "";
  description: any = "";
  authorName: any = "";
  authorTitle: any = "";

  quoteSub: Subscription = new Subscription();

  constructor(public quoteService: QuoteService, public route: ActivatedRoute, private router: Router){

  }


  ngOnInit(){


    this.quoteService.getsetting().subscribe((data :any) => {

      this.isLoading = false;
      if(data.disableEdit){
        this.router.navigate(["/quotes"]);
      }

      this.quoteSub = this.quoteService.getquotesUpdateListener().subscribe(
        isQuote => {
          this.isLoading = false;
        }
      );

      this.route.paramMap.subscribe((paramMap: ParamMap) => {
        if(paramMap.has('id')){
          this.id = paramMap.get('id');
          this.isLoading = true;

          this.quoteService.getquote(this.id).subscribe((data :any) => {
            this.isLoading = false;
            this.id = data.id,
            this.quoteText = data.quoteText,
            this.description = data.description,
            this.authorName = data.authorName,
            this.authorTitle = data.authorTitle
            });
          }
        });

      });

    }

  onEditQuote(form: NgForm){

    if(form.invalid){
      return;
    }

    this.isLoading = true;

    this.quoteService.EditQuote(form.value);

   }

   ngOnDestroy(){
    this.quoteSub.unsubscribe();
   }

}
