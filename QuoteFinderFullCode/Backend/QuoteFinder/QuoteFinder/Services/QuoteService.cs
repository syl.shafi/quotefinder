﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuoteFinder.Authentication;
using QuoteFinder.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using QuoteFinder.DataContext;
using QuoteFinder.Models;
using Microsoft.EntityFrameworkCore;

namespace QuoteFinder.Services
{
    public class QuoteService : IQuoteRepository<Quote>
    {
        readonly QuoteDBContext _QuoteDBContext;

        public QuoteService(QuoteDBContext context)
        {
            _QuoteDBContext = context;
        }
        public IEnumerable<Quote> GetAll(string search, string LogedUserRole, string CurrentEntryByUserId, VisibilityStatus VisibilityStatus, int index, int noOfResults)
        {
            var Quotes = _QuoteDBContext.Quotes.Where(r => r.VisibilityStatus == VisibilityStatus).OrderByDescending(r => r.EntryDate).AsQueryable();

            if (LogedUserRole != UserRoles.Admin)
            {
                Quotes = Quotes.Where(r => r.EntryByUserId == CurrentEntryByUserId).AsQueryable();
            }

            if (search == null || search == "")
            {
                return Quotes.Skip(index).Take(noOfResults).ToList();
            }
            else
            {
                return Quotes.Where(r=> r.EntryByUserName.Contains(search) || r.QuoteText.Contains(search)).Skip(index).Take(noOfResults).ToList();
            }

        }

        public int GetCount(string search, string LogedUserRole, string CurrentEntryByUserId, VisibilityStatus VisibilityStatus)
        {
            var Quotes = _QuoteDBContext.Quotes.Where(r => r.VisibilityStatus == VisibilityStatus).OrderByDescending(r => r.EntryDate).AsQueryable();

            if (LogedUserRole != UserRoles.Admin)
            {
                Quotes = Quotes.Where(r => r.EntryByUserId == CurrentEntryByUserId).AsQueryable();
            }

            if (search == null || search == "")
            {
                return Quotes.Count();
            }
            else
            {
                return Quotes.Where(r => r.EntryByUserName.Contains(search) || r.QuoteText.Contains(search)).Count();
            }

        }


        public Quote Get(int id, string LogedUserRole, string CurrentUserId)
        {

            if(LogedUserRole == UserRoles.Admin)
            {
                return _QuoteDBContext.Quotes.FirstOrDefault(e => e.Id == id);
            }
            else
            {
                return _QuoteDBContext.Quotes
                  .FirstOrDefault(e => e.Id == id && e.EntryByUserId == CurrentUserId);
            }
        }


        public void Add(Quote entity)
        {
            _QuoteDBContext.Quotes.Add(entity);
            _QuoteDBContext.SaveChanges();
        }


        public void Update(Quote QuoteToUpdate)
        {
            _QuoteDBContext.Entry(QuoteToUpdate).State = EntityState.Modified;
            _QuoteDBContext.SaveChanges();
        }
        public void Delete(Quote Quote)
        {
            _QuoteDBContext.Quotes.Remove(Quote);
            _QuoteDBContext.SaveChanges();
        }


    }
}