﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuoteFinder.Authentication;
using QuoteFinder.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using QuoteFinder.DataContext;
using QuoteFinder.Models;
using Microsoft.EntityFrameworkCore;

namespace QuoteFinder.Services
{
    public class SettingService : ISettingRepository<Setting>
    {
        readonly QuoteDBContext _QuoteDBContext;

        public SettingService(QuoteDBContext context)
        {
            _QuoteDBContext = context;
        }
        public Setting Get()
        {
            return _QuoteDBContext.Settings.FirstOrDefault();

        }

        public void Add(Setting entity)
        {
            _QuoteDBContext.Settings.Add(entity);
            _QuoteDBContext.SaveChanges();
        }


        public void Update(Setting Setting)
        {
            _QuoteDBContext.Entry(Setting).State = EntityState.Modified;
            _QuoteDBContext.SaveChanges();
        }



    }
}