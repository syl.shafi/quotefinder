﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using QuoteFinder.Authentication;
using QuoteFinder.Models;

namespace QuoteFinder.DataContext
{

    public class QuoteDBContext : IdentityDbContext<ApplicationUser>
    {
        public QuoteDBContext(DbContextOptions<QuoteDBContext> options)
            : base(options)
        {
        }
        public DbSet<Quote> Quotes { get; set; }
        public DbSet<Setting> Settings { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
        }
    }



}





