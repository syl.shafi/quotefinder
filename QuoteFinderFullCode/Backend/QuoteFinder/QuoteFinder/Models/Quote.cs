﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteFinder.Models
{

    public enum VisibilityStatus
    {

        Active,
        Trashed

    }

    public class Quote
    {

        public int Id { get; set; }
        public string QuoteText { get; set; }
        public string Description { get; set; }
        public string AuthorName { get; set; }
        public string AuthorTitle { get; set; }
        public VisibilityStatus VisibilityStatus { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public string EntryByUserId { get; set; }
        public string EntryByUserName { get; set; }
        public string ModifiedByUserId { get; set; }
        public string ModifiedByUserName { get; set; }


    }
}
