﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteFinder.Models
{
    public class Setting
    {
        public int Id { get; set; }
        public bool DisableAdd { get; set; }
        public bool DisableEdit { get; set; }
        public bool DisableDelete { get; set; }
        public bool DisableView { get; set; }

    }
}
