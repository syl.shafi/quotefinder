﻿using QuoteFinder.Models;
using Microsoft.AspNetCore.Identity;
using System;

namespace QuoteFinder.Authentication
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
        public DateTime DOB { get; set; }
        public string Address { get; set; }
        public bool Blocked { get; set; }

    }
}
