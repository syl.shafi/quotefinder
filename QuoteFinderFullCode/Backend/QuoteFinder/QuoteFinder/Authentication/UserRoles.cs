﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteFinder.Authentication
{
    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string Normal = "Normal";
    }
}
