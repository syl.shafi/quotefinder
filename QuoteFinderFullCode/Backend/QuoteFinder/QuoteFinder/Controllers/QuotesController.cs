﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using QuoteFinder.Authentication;
using QuoteFinder.Models;
using QuoteFinder.Repository;
//using QuoteFinder.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;

namespace QuoteFinder.Controllers
{

    [Route("api/Quote")]
    [ApiController]
    public class QuoteController : ControllerBase
    {

        private readonly IQuoteRepository<Quote> _quoteRepository;
        private readonly ISettingRepository<Setting> _settingRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public QuoteController(IQuoteRepository<Quote> quoteRepository,
            ISettingRepository<Setting> settingRepository,
            UserManager<ApplicationUser> userManager)
        {
            _quoteRepository = quoteRepository;
            _settingRepository = settingRepository;
            this._userManager = userManager;
        }

        public Setting GetCurrentSetting()
        {

            Setting Setting = _settingRepository.Get();

            if (Setting == null)
            {
                return new Setting();
            }

            return Setting;
        }

        public async Task<string> getCurrentUserRole()
        {
            var UserName = User.Identity.Name;
            var user = _userManager.Users.FirstOrDefault(r => r.UserName == UserName);
            var userRoles = await _userManager.GetRolesAsync(user);
            string role = userRoles.FirstOrDefault();
            return role;
        }

        public string getCurrentUserId()
        {
            var UserName = User.Identity.Name;
            var user = _userManager.Users.FirstOrDefault(r => r.UserName == UserName);
            return user.Id;
        }


        // GET: api/Quote
        [Authorize]
        [Route("get-all")]
        [HttpGet]
        public async Task<IActionResult> GetAsync(string search, VisibilityStatus VisibilityStatus, int index, int noOfResults)
        {
            Setting setting = GetCurrentSetting();

            if (setting.DisableView)
            {
                return BadRequest("Action Disabled.");
            }

            string role = await getCurrentUserRole();
            string userId = getCurrentUserId();

            IEnumerable<Quote> Quotes =  _quoteRepository.GetAll(search, role, userId, VisibilityStatus, index-1, noOfResults);

            return Ok(new { quotes = Quotes, maxquotes = _quoteRepository.GetCount(search, role, userId, VisibilityStatus) });

        }


        // GET: api/Quote/5
        [Authorize]
        [Route("get-single/{id}")]
        [HttpGet(Name = "get-single")]
        public async Task<IActionResult> GetAsync(int id)
        {

            Setting setting = GetCurrentSetting();

            if (setting.DisableView)
            {
                return BadRequest("Action Disabled.");
            }

            string role = await getCurrentUserRole();
            string userId =  getCurrentUserId();

            Quote Quote =  _quoteRepository.Get(id, role, userId);

            if (Quote == null)
            {
                return NotFound("The Quote record couldn't be found.");
            }

            return Ok(Quote);
        }

        // POST: api/Quote
        [Authorize]
        [Route("add")]
        [HttpPost]
        public IActionResult PostAsync([FromBody] Quote Quote)
        {
            Setting setting = GetCurrentSetting();

            if (setting.DisableAdd)
            {
                return BadRequest("Action Disabled.");
            }


            if (Quote == null)
            {
                return BadRequest("Quote is null.");
            }

            string userId = getCurrentUserId();
            var UserName = User.Identity.Name;
            DateTime currDate = DateTime.Now;

            Quote dbQuote = new Quote
            {
                QuoteText = Quote.QuoteText,
                Description = Quote.Description,
                AuthorName = Quote.AuthorName,
                AuthorTitle = Quote.AuthorTitle,
                VisibilityStatus = VisibilityStatus.Active,
                EntryDate = currDate,
                EntryByUserId = userId,
                EntryByUserName = UserName,
                LastUpdatedAt = currDate,
                ModifiedByUserId = userId,
                ModifiedByUserName = UserName

            };

            _quoteRepository.Add(dbQuote);

            return Ok(new { Id = dbQuote.Id });
        }

        // PUT: api/Quote/5
        [Authorize]
        [Route("edit/{id}")]
        [HttpPut]
        public async Task<IActionResult> PutAsync(int id, [FromBody] Quote Quote)
        {
            Setting setting = GetCurrentSetting();

            if (setting.DisableEdit)
            {
                return BadRequest("Action Disabled.");
            }

            if (Quote == null)
            {
                return BadRequest("Quote is null.");
            }

            string role = await getCurrentUserRole();
            string userId = getCurrentUserId();
            var UserName = User.Identity.Name;
            DateTime currDate = DateTime.Now;

            Quote QuoteToUpdate = _quoteRepository.Get(id, role, userId);


            if (QuoteToUpdate == null)
            {
                return NotFound("The Quote record couldn't be found.");
            }

            QuoteToUpdate.QuoteText = Quote.QuoteText;
            QuoteToUpdate.Description = Quote.Description;
            QuoteToUpdate.AuthorName = Quote.AuthorName;
            QuoteToUpdate.AuthorTitle = Quote.AuthorTitle;
            QuoteToUpdate.VisibilityStatus = QuoteToUpdate.VisibilityStatus;
            QuoteToUpdate.LastUpdatedAt = currDate;
            QuoteToUpdate.ModifiedByUserId = userId;
            QuoteToUpdate.ModifiedByUserName = UserName;


            _quoteRepository.Update(QuoteToUpdate);
            return NoContent();
        }


        [Authorize]
        [Route("set-visibility")]
        [HttpPost]
        public async Task<IActionResult> SetVisibility(int id, VisibilityStatus VisibilityStatus)
        {

            Setting setting = GetCurrentSetting();


            if (setting.DisableEdit)
            {
                return BadRequest("Action Disabled.");
            }


            string role = await getCurrentUserRole();
            string userId = getCurrentUserId();
            var UserName = User.Identity.Name;
            DateTime currDate = DateTime.Now;

            Quote QuoteToUpdate = _quoteRepository.Get(id, role, userId);


            if (QuoteToUpdate == null)
            {
                return NotFound("The Quote record couldn't be found.");
            }


            QuoteToUpdate.QuoteText = QuoteToUpdate.QuoteText;
            QuoteToUpdate.Description = QuoteToUpdate.Description;
            QuoteToUpdate.AuthorName = QuoteToUpdate.AuthorName;
            QuoteToUpdate.AuthorTitle = QuoteToUpdate.AuthorTitle;
            QuoteToUpdate.VisibilityStatus = VisibilityStatus;
            QuoteToUpdate.LastUpdatedAt = currDate;
            QuoteToUpdate.ModifiedByUserId = userId;
            QuoteToUpdate.ModifiedByUserName = UserName;

            _quoteRepository.Update(QuoteToUpdate);

            return NoContent();

        }

        // DELETE: api/Quote/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {

            Setting setting = GetCurrentSetting();

            if (setting.DisableDelete)
            {
                return BadRequest("Action Disabled.");
            }

            string role = await getCurrentUserRole();
            string userId = getCurrentUserId();

            Quote Quote = _quoteRepository.Get(id, role, userId);
            if (Quote == null)
            {
                return NotFound("The Quote record couldn't be found.");
            }
            _quoteRepository.Delete(Quote);
            return NoContent();
        }


        // GET: api/Quote/5
        [Authorize]
        [Route("get-settings")]
        [HttpGet]
        public IActionResult GetSettings()
        {

            Setting Setting = _settingRepository.Get();

            if (Setting == null)
            {
                return Ok(new Setting());
            }

            return Ok(Setting);
        }


        [Authorize(Roles = UserRoles.Admin)]
        [Route("settings")]
        [HttpPost]
        public IActionResult Settings([FromBody] Setting Setting)
        {

            if (Setting == null)
            {
                return BadRequest("Setting is null.");
            }

            Setting SettingToUpdate = _settingRepository.Get();

            if (SettingToUpdate == null)
            {
                _settingRepository.Add(Setting);
                return NoContent();
            }

            SettingToUpdate.DisableAdd = Setting.DisableAdd;
            SettingToUpdate.DisableEdit = Setting.DisableEdit;
            SettingToUpdate.DisableDelete = Setting.DisableDelete;
            SettingToUpdate.DisableView = Setting.DisableView;

            _settingRepository.Update(SettingToUpdate);
            return NoContent();
        }



    }
}