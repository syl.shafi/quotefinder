﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteFinder.Repository
{
    public interface ISettingRepository<TEntity>
    {
        TEntity Get();
        void Add(TEntity entity);
        void Update(TEntity dbEntity);

    }
}
