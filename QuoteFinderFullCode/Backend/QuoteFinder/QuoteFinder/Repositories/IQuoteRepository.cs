﻿using QuoteFinder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteFinder.Repository
{
    public interface IQuoteRepository<TEntity>
    {
        IEnumerable<TEntity> GetAll(string search, string LogedUserRole, string CurrentUserId, VisibilityStatus VisibilityStatus, int index, int noOfResults);
        int GetCount(string search, string LogedUserRole, string CurrentUserId, VisibilityStatus VisibilityStatus);
        TEntity Get(int id, string LogedUserRole, string CurrentUserId);
        void Add(TEntity entity);
        void Update(TEntity dbEntity);
        void Delete(TEntity entity);

    }
}
